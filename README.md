﻿Kick Vice
======
Kick Vice script for Windows, which kicks Vice64 emulator from NotePad++.

Kicking files in Vice after compiling in Notepad++ must be executed by this script as for this command:

		NPP_RUN "C:\Program Files\WinVice-2.4-x64\x64.exe" "$(CURRENT_DIRECTORY)/$(NAME_PART).prg"

there is a bug - when `$(CURRENT_DIRECTORY)` contains spaces, then argument is passed to Vice unproperly.


If you want to run monitor commands like attaching labels, just set `MONITORCOMMANDS` environment variable
which points to text file with all commands. Example of the file:

		ll "<my_path>/symbols.l"

		
Example of executing the whole compile process under Notepad++:


		NPP_SAVE
		ENV_SET OUTFILE="F:\COMMOCORE\PROJECTS\MEONLAWEL\build\meonlawel.prg"
		"C:\Program Files\64tass-1.51.675\64tass.exe" -a "F:\COMMOCORE\PROJECTS\MEONLAWEL\meonlawel.asm" -l "F:\COMMOCORE\PROJECTS\MEONLAWEL\build\symbols.l" -o $(SYS.OUTFILE) 
		if $(EXITCODE) > 0 goto END
		ENV_SET MONITORCOMMANDS=F:\COMMOCORE\PROJECTS\MEONLAWEL\monitor-commands.txt
		NPP_RUN F:\COMMOCORE\TOOLS\kick-vice\kick-vice.vbs
		:END


